# Best practices for Flask

Also check
* global best practices
* [Python best practices](https://code.europa.eu/digit-c4/dev/python-best-practices)

## Boilerplate
This project provides a Flask API boilerplate
```bash
# api names are kebab-case, and must be "-api" suffixed
PROJECT_NAME=my-app-api
git clone git@code.europa.eu:digit-c4/dev/flask-best-practices.git $PROJECT_NAME\
  && cd $PROJECT_NAME \
  && rm -rf .git \
  && git init
cd ./api/
# Enable python virtual environment
python3 -m venv venv \
  && source venv/bin/activate
# Make sure dependencies are installed
poetry install
# Get started
flask --app flask_best_practices_api.wsgi run
```

## Testing
Ensure test dependencies are in
```bash
poetry install --only test
```

### Static tests
```bash
# Using Flake8 for lint
poetry run flake8 src
# Enforce styling
poetry run autopep8 src
# Using pytype for type checking
```

### Unit tests
```shell
# using Pytest, with support of unittest out of the box
poetry run pytest
```
