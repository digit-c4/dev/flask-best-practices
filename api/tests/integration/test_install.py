import logging
import os
import time
import unittest
import subprocess
from uuid import uuid4

import requests


class TestInstall(unittest.TestCase):

    def test_install_dependencies(self):
        root = os.path.normpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../..'))
        process = subprocess.Popen([
            "docker", "run", "--rm",
            "-v", f"{root}:/api",
            "--workdir", "/api",
            "code.europa.eu:4567/digit-c4/dev/python-best-practices/python-poetry:3.11-alpine", "/bin/sh", "-c",
            "poetry install"
        ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        self.assertEqual(0, process.returncode, f"Stdout: {stdout}, Stderr: {stderr}")

    def test_run_flask(self):
        root = os.path.normpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../..'))
        container_name = str(uuid4())
        try: 
            process = subprocess.Popen([
                "docker", "run", "-d", "--rm",
                "--name", container_name,
                "--volume", f"{root}:/api",
                "--workdir", "/api",
                "--publish", "5000:5000",
                "code.europa.eu:4567/digit-c4/dev/python-best-practices/python-poetry:3.11-alpine", "/bin/sh", "-c",
                "poetry install && flask --app flask_best_practices_api.wsgi run --host=0.0.0.0"
            ],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            stdout, stderr = process.communicate()
            stdout, stderr
            self.assertEqual(0, process.returncode)
            retry = 0
            response = None
            while response is None or not response.ok:
                try:
                    logging.warn("Retry: "+retry)
                    response = requests.get('http://localhost:5000')
                    response.raise_for_status()
                except:
                    if retry > 3:
                        raise
                    time.sleep(1)
                
                retry += 1
                if retry > 3:
                    raise "Too many retries"
        
        finally:
            process = subprocess.Popen(["docker", "rm", "--force", container_name],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)


if __name__ == '__main__':
    unittest.main()
