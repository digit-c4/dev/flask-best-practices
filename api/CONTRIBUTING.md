```
python3 -m venv venv
source venv/bin/activate
poetry install --with test
```

## Testing
```
poetry run pytest tests/unit
```
