import os

from flask import Flask as FlaskSource
from flask import Blueprint
from flasgger import Swagger

nms_blueprint = Blueprint('nms', __name__)


@nms_blueprint.route("/_health")
def _health():
    [sysname, nodename, release, version, machine] = os.uname()

    return {"os": {
        "pid": os.getpid(),
        "parent_pid": os.getppid(),
        "uid": os.getuid(),
        "cpu": os.cpu_count(),
        "load": os.getloadavg(),
        "sysname": sysname,
        "nodename": nodename,
        "release": release,
        "version": version,
        "machine": machine,
    }}


class Flask(FlaskSource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.register_blueprint(nms_blueprint)

        swagger_config = {
            "headers": [],
            "specs": [
                {
                    "endpoint": '_openapi',
                    "route": '/_openapi.json',
                    "rule_filter": lambda rule: True,  # all in
                    "model_filter": lambda tag: True,  # all in
                }
            ],
            "swagger_ui": True,
            "specs_route": "/_swagger/"
        }
        Swagger(self, config=swagger_config)
