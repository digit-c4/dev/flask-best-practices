from flask import Response, json


class JsonResponse(Response):
    def __init__(
            self,
            response,
            status,
            headers=None,
            content_type=None,
            direct_passthrough: bool = False,
    ):
        response = json.dumps(response)
        mimetype = "application/json"
        super().__init__(
            response=response,
            status=status,
            headers=headers,
            mimetype=mimetype,
            content_type=content_type,
            direct_passthrough=direct_passthrough
        )
