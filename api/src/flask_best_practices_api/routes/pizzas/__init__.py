import uuid

from flask import Blueprint, request
from nms_flask_common import JsonResponse

bp = Blueprint('pizzas', __name__)

pizzas = []


@bp.route('/', methods=['POST'])
def create():
    body = request.json
    body['id'] = uuid.uuid4()
    pizzas.append(body)
    return JsonResponse(body, status=201)


@bp.route('/', methods=['GET'])
def ls():
    return pizzas
