import uuid

from flask import Blueprint, request
from nms_flask_common import JsonResponse

bp = Blueprint('orders', __name__)

orders = {}


@bp.route('/', methods=['POST'])
def create():
    body = request.json
    body['id'] = str(uuid.uuid4())
    orders[body['id']] = body
    return JsonResponse(body, status=201)


@bp.route('/', methods=['GET'])
def ls():
    return list(orders.values())


@bp.route('/<order_id>', methods=['GET'])
def get(order_id):
    if order_id not in orders:
        return JsonResponse({}, status=404)
    return orders[order_id]
