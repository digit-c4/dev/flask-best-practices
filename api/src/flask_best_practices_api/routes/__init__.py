from flask import Blueprint

from .orders import bp as orders_bp
from .pizzas import bp as pizzas_bp

bp = Blueprint('root', __name__)

bp.register_blueprint(orders_bp, url_prefix='orders')
bp.register_blueprint(pizzas_bp, url_prefix='pizzas')


@bp.route('/')
def index():
    return {"hello": "<p>Hello, Blabetiblou!</p>"}
