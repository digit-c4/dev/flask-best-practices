import logging

from nms_common import nms_logging
from nms_flask_common import Flask

from .routes import bp as root_bp

api = Flask("flask-best-practices")

nms_logging.basicConfig(owner_name="dev", app_name=api.name, app_type="api")
logging.info("This message matches the standard formatting")

api.register_blueprint(root_bp, url_prefix='/')

def get_openapi():
    with api.app_context():
        with api.test_client() as client:
            return client.get('/_openapi.json').get_data()
